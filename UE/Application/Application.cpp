#include "Application.hpp"

namespace ue
{

Application::Application(common::PhoneNumber phoneNumber,
                         common::ILogger &iLogger)
    : logger(iLogger, "[APP] ")
{
    logger.logInfo("Started");
}

Application::~Application()
{
    logger.logInfo("Stopped");
}

}
