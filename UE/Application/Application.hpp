#pragma once

#include "Logger/PrefixedLogger.hpp"
#include "Messages/PhoneNumber.hpp"

namespace ue
{

using common::PhoneNumber;
using common::ILogger;

class Application
{
public:
    Application(PhoneNumber phoneNumber,
                ILogger& iLogger);
    ~Application();

private:
    common::PrefixedLogger logger;
};

}
