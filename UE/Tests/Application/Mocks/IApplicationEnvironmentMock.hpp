#pragma once

#include <gmock/gmock.h>

#include "IApplicationEnvironment.hpp"

namespace ue
{

struct IApplicationEnvironmentMock : public IApplicationEnvironment
{
    IApplicationEnvironmentMock();
    ~IApplicationEnvironmentMock() override;

    MOCK_METHOD0(getUeGui, IUeGui&());
    MOCK_METHOD0(getTransportToBts, common::ITransport&());
    MOCK_METHOD0(getLogger, common::ILogger&());
    MOCK_METHOD0(startMessageLoop, void());
    MOCK_CONST_METHOD0(getMyPhoneNumber, common::PhoneNumber());
    MOCK_CONST_METHOD2(getProperty, int32_t(const std::string &name, int32_t defaultValue));
};

}
