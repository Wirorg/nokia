#pragma once

#include <gmock/gmock.h>
#include "IUeGui.hpp"
#include "UeGui/IListViewMode.hpp"
#include "UeGui/ISmsComposeMode.hpp"
#include "UeGui/IDialMode.hpp"
#include "UeGui/ICallMode.hpp"
#include "UeGui/ITextMode.hpp"

namespace ue
{

struct IUeGuiMock : public IUeGui
{
    IUeGuiMock();
    ~IUeGuiMock() override;

    MOCK_METHOD1(setCloseGuard, void(CloseGuard closeGuard));
    MOCK_METHOD1(setAcceptCallback, void(Callback));
    MOCK_METHOD1(setRejectCallback, void(Callback));
    MOCK_METHOD1(setTitle, void(const std::string &title));
    MOCK_METHOD0(showConnected, void());
    MOCK_METHOD0(showConnecting, void());
    MOCK_METHOD0(showNotConnected, void());
    MOCK_METHOD0(showNewSms, void());
    MOCK_METHOD1(showPeerUserNotAvailable, void(common::PhoneNumber));

    MOCK_METHOD0(setListViewMode, IListViewMode&());
    MOCK_METHOD0(setSmsComposeMode, ISmsComposeMode&());
    MOCK_METHOD0(setDialMode, IDialMode&());
    MOCK_METHOD0(setCallMode, ICallMode&());
    MOCK_METHOD0(setAlertMode, ITextMode&());
    MOCK_METHOD0(setViewTextMode, ITextMode&());
};

class IListViewModeMock : public IUeGui::IListViewMode
{
public:
    IListViewModeMock();
    ~IListViewModeMock() override;

    MOCK_CONST_METHOD0(getCurrentItemIndex, OptionalSelection());
    MOCK_METHOD2(addSelectionListItem, void(const std::string &label, const std::string &tooltip));
    MOCK_METHOD0(clearSelectionList, void());
};

class ITextModeMock : public IUeGui::ITextMode
{
public:
    ITextModeMock();
    ~ITextModeMock() override;

    MOCK_METHOD1(setText, void(const std::string &text));
};

class ISmsComposeModeMock : public IUeGui::ISmsComposeMode
{
public:
    ISmsComposeModeMock();
    ~ISmsComposeModeMock() override;

    MOCK_CONST_METHOD0(getPhoneNumber, PhoneNumber());
    MOCK_CONST_METHOD0(getSmsText, std::string());
    MOCK_METHOD0(clearSmsText, void());
};

class ICallModeMock : public IUeGui::ICallMode
{
public:
    ICallModeMock();
    ~ICallModeMock() override;

    MOCK_METHOD1(appendIncomingText, void(const std::string &text));
    MOCK_METHOD0(clearOutgoingText, void());
    MOCK_CONST_METHOD0(getOutgoingText, std::string());
};

class IDialModeMock : public IUeGui::IDialMode
{
public:
    IDialModeMock();
    ~IDialModeMock() override;

    MOCK_CONST_METHOD0(getPhoneNumber, PhoneNumber());
};

}
