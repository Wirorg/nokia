#include "Application.hpp"
#include "ApplicationEnvironmentFactory.hpp"

int main(int argc, char* argv[])
{
    using namespace ue;
    using namespace std::chrono_literals;

    auto appEnv = ue::createApplicationEnvironment(argc, argv);
    auto& logger = appEnv->getLogger();
    auto phoneNumber = appEnv->getMyPhoneNumber();

    Application app(phoneNumber, logger);
    appEnv->startMessageLoop();
}

